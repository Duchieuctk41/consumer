package main

import (
	"bytes"
	"github.com/sirupsen/logrus"
	"log"
	"net/http"
	"rabbitmq/utils"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	logrus.Infof("lo vao consumer")
	//connect rabbitmq
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	// connect channel
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	// declare exchange
	err = ch.ExchangeDeclare(
		"ms-elasticsearch", // name
		"topic",            // type
		true,               // durable
		false,              // auto-deleted
		false,              // internal
		false,              // no-wait
		nil,                // arguments
	)
	failOnError(err, "Failed to declare an exchange")

	// declare queue
	q, err := ch.QueueDeclare(
		utils.TOPIC_UPDATE_EMAIL_ORDER_RECENT, // name: update email in order recent when update email_notification
		//utils.TOPIC_SEND_EMAIL_ORDER, // name: send email order
		//utils.TOPIC_SET_USER_GUIDE, // name: set user guide in meta-data
		//utils.TOPIC_UPDATE_DEBT_AMOUNT, // update debt_amount in business-management
		false,                                 // durable
		false,                                 // delete when unused
		true,                                  // exclusive
		false,                                 // no-wait
		nil,                                   // arguments
	)
	failOnError(err, "Failed to declare a queue")

	// queue bind topic
	err = ch.QueueBind(
		q.Name,                                // queue name

		utils.TOPIC_UPDATE_EMAIL_ORDER_RECENT, // routing key: update email in order recent when update email_notification
		//utils.TOPIC_SEND_EMAIL_ORDER, // routing key: send email order
		//utils.TOPIC_SET_USER_GUIDE, // name: set user guide in meta-data
		//utils.TOPIC_UPDATE_DEBT_AMOUNT, // update debt_amount in business-management
		"ms-elasticsearch",                    // exchange
		false,
		nil)
	failOnError(err, "Failed to bind a queue")

	// declare consume take body from queue
	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto ack
		false,  // exclusive
		false,  // no local
		false,  // no wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")
	forever := make(chan bool)
	go func() {
		for d := range msgs {
			logrus.Infof("vao day")

			// finan-order
			req, err := http.Post(utils.FINAN_ORDER+"/api/v1/consumer", "application/json", bytes.NewReader(d.Body))

			// ms-business-management
			//req, err := http.Post(utils.MS_BUSINESS_MANAGEMENT+"/api/consumer", "application/json", bytes.NewReader(d.Body))

			// ms-meta-data
			//req, err := http.Post(utils.MS_META_DATA+"/api/consumer", "application/json", bytes.NewReader(d.Body))

			// ms-product-management
			//req, err := http.Post(utils.MS_PRODUCT_MANAGEMENT+"/api/consumer", "application/json", bytes.NewReader(d.Body))

			if err != nil {
				return
			}
			logrus.Infof("log success: %v", req)
		}
	}()

	log.Printf(" [*] Waiting for logs. To exit press CTRL+C")
	<-forever
}
