package main

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"log"
	"rabbitmq/utils"
)

type SyncData struct {
	Topic  string `json:"topic"`
	Body   string `json:"body"`
	Limit  int    `json:"limit"`
	Offset int    `json:"offset"`
}

type ConsumerResponse struct {
	Topic   string `json:"topic"`
	Payload string `json:"payload"`
}

func main() {
	r := gin.Default()
	r.POST("/events", CallSyncData)
	logrus.Infof("server is running in localhost" + utils.RABBIT_MQ)
	r.Run(utils.RABBIT_MQ)
}

func CallSyncData(c *gin.Context) {
	var data SyncData
	c.BindJSON(&data)

	// convert to consumerResponse
	res := ConsumerResponse{
		Topic:   data.Topic,
		Payload: data.Body,
	}

	//logrus.Infof("log: %v", data)

	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	// connect channel
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	// declare exchange
	err = ch.ExchangeDeclare(
		"ms-elasticsearch", // exchange name
		"topic",            // type
		true,               // durable
		false,              // auto-deleted
		false,              // internal
		false,              // no-wait
		nil,                // arguments
	)
	failOnError(err, "Failed to declare an exchange")

	body, err := json.Marshal(res)
	err = ch.Publish(
		"ms-elasticsearch",                    // exchange
		utils.TOPIC_UPDATE_EMAIL_ORDER_RECENT, // routing key: update email in order recent when update email_notification
		//utils.TOPIC_SEND_EMAIL_ORDER, // routing key: send email order
		//utils.TOPIC_SET_USER_GUIDE, // routing key: tutorial flow
		//utils.TOPIC_UPDATE_DEBT_AMOUNT, // update debt_amount in business-management
		false,                                 // mandatory
		false,                                 // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        body,
		})
	failOnError(err, "Failed to publish a message")

	//log.Printf(" [x] Sent %s", body)
}
func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
